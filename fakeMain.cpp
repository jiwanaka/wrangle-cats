
#include <iostream>
#include <cassert>
#include <list>


#include "cat.hpp"
#include "node.hpp"
#include "list.hpp"
#include "queue.hpp"

using namespace std;

int main() {
	cout << "Hello World!" << endl;
	
	Cat::initNames();
	
	Cat* newCat = Cat::makeCat();
	
	cout << newCat->getInfo() << endl;

    cout<< "testing stuff" << endl;
    
	DoubleLinkedList list = DoubleLinkedList();

	assert( list.validate() );

	for( int i = 0 ; i < 100 ; i++ ) {

		list.push_front( Cat::makeCat() );

		assert( list.validate() );

        //cout<< "abc" << endl;
	}

	cout<< list.get_first() << endl;
	cout<< list.get_last() << endl;
	//cout<< list.get_next() << endl;
	//cout<< list.get_prev() << endl;

    cout<< "another test" << endl;

    	for( int i = 0 ; i < 100 ; i++ ) {

		list.pop_front();

		assert( list.validate() );
	}

    cout<< "3rd test" << endl;

    	for( int i = 0 ; i < 100 ; i++ ) {

		list.push_back( Cat::makeCat() );

		assert( list.validate() );
	}
	cout<< list.size() << endl;
	cout<< list.empty() << endl;

    cout<< "4th test" << endl;
	for( int i = 0 ; i < 100 ; i++ ) {

		list.pop_back();

		assert( list.validate() );
	}

    cout<< list.size() << endl;
	cout<< list.empty() << endl;
}
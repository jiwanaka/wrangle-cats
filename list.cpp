///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.cpp
/// @version 2.0
///
/// Unit test for this lab
///
/// @author @todo Jayson Iwanaka <@todo jiwanaka@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   @todo 20_04_2021
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <cassert>


#include "node.hpp"
#include "list.hpp"

using namespace std;

    const bool DoubleLinkedList::empty() const {
        if(head == nullptr && tail == nullptr)
        return true;
        else
        return false; 
    }

    const bool DoubleLinkedList::isIn(Node* aNode) const{
        Node* currentNode = head;

        while(currentNode != nullptr){
            if(aNode == currentNode)
                return true;
            currentNode = currentNode->next;
        }
        return false;
    }

    /* void DoubleLinkedList::push_front( Node* newNode ){
        newNode->next = nullptr;
        newNode->next = head;
        head = newNode;
        nodeCount++;
        } */

    void DoubleLinkedList::push_front( Node* newNode ){
        if(newNode == nullptr){
            return;
        }

        if(head != nullptr){
            newNode->next = head;
            newNode->prev = nullptr;
            head->prev = newNode;
            head = newNode;
        } else {
            newNode->next = nullptr;
            newNode->prev = nullptr;
            head = newNode;
            tail = newNode;
        }

        nodeCount++;
        //cout<<"+1 haha"<<endl;
       validate();

    }
/*
    void DoubleLinkedList::push_back( Node* newNode ){
        if(newNode == nullptr)
        return;

        if(tail != nullptr){
            newNode->next = nullptr;
            newNode->prev = tail;
            tail->next = newNode;
            tail = newNode;
        } else {
            newNode->next = nullptr;
            newNode->prev = nullptr;
            head = newNode;
            tail = newNode;
        }

        nodeCount++;
        cout<<"+1 doydoi"<<endl;
       // validate();
    }
*/

    void DoubleLinkedList::push_back(Node* newNode){
            Node* n = newNode;

    if (head == nullptr)
        head = n; //this was your main problem!

    if (tail != nullptr)
        tail->next = n;

    n->next = nullptr;
    n->prev = tail;
    tail = n;

    nodeCount++;
    validate();
    }


     Node* DoubleLinkedList::pop_front(){
         if(head == nullptr)
         return nullptr;
        Node *tmp = head;
        head = head->next;

        if (head){
            head->prev = nullptr;
        } else {
            tail = nullptr;
        }
    nodeCount--;
   // cout<<"-1 sadFace"<<endl;
    return tmp;
   
} 

    Node* DoubleLinkedList::pop_back(){
        if(tail == nullptr)
        return nullptr;
        
        Node* tmp = tail;
        tail = tail->prev;

        if (tail){
            tail->next = nullptr;
        } else {
            head = nullptr;
        }
    nodeCount--;
  //  cout<<"-1 :("<<endl;
    return tmp;

    }



    

    Node* DoubleLinkedList:: get_first() const {
        return head;
    }

    Node* DoubleLinkedList::get_last() const{
        return tail;
    }

    Node* DoubleLinkedList::get_next( const Node* currentNode ) const{
       return currentNode->next;
    }

    Node* DoubleLinkedList::get_prev(const Node* currentNode) const{
        return currentNode->prev;
    }

    void DoubleLinkedList::insert_after(Node* currentNode, Node* newNode){
        if(currentNode == nullptr && head == nullptr){
            push_front(newNode);
            return;
        }
        if(currentNode != nullptr && head == nullptr){
            assert(false);
        }
        if(currentNode == nullptr && head != nullptr){
            assert(false);
        }

        assert(currentNode != nullptr && head != nullptr);

        assert(isIn(currentNode));

        assert(!isIn(newNode));

        if(tail != currentNode){
            newNode->next = currentNode->next;
            currentNode->next = newNode;
            newNode->prev = currentNode;
            newNode->next->prev = newNode;
        } else {
            push_back(newNode);
        }

        validate();
    }

    void DoubleLinkedList::insert_before(Node* currentNode, Node* newNode){
            if(currentNode == nullptr && tail == nullptr){
            push_back(newNode);
            return;
        }
        if(currentNode != nullptr && tail == nullptr){
            assert(false);
        }
        if(currentNode == nullptr && tail != nullptr){
            assert(false);
        }

        assert(currentNode != nullptr && tail != nullptr);

        assert(isIn(currentNode));

        assert(!isIn(newNode));

        if(head != currentNode){
            newNode->prev = currentNode->prev;
            currentNode->prev = newNode;
            newNode->next = currentNode;
            newNode->prev->next = newNode;
        } else {
            push_front(newNode);
        }

        validate();
    }

 ///*   
    void DoubleLinkedList::swap(Node* node1, Node* node2){
        if (node1 == node2){
            return;
        }


        Node* temp = node1;
        node1 = node2;
        node2 = temp;
        

    }
//*/
/*
    void DoubleLinkedList::swap(Node* node1, Node* node2){

        if(node1 == node2){
        return;
        }

        Node* node1_left = node1->prev;
        Node* node1_right = node1->next;
        Node* node2_left = node2->prev;
        Node* node2_right = node2->next;

        bool isAdjoining = (node1_right == node2);

        if(!isAdjoining){
            node1->prev = node2->prev;
        }else{
            node1->prev = node2;
        } if(node1 != head){
            node1_left->next = node2;
            node2->prev = node1_left;
        } else {
            head = node2;
            node2->prev = nullptr;
        }
       if(!isAdjoining){
            node2->next = node1_right;
       } else {
            node2->next = node1;
        }
        if(node2 != tail){
            node2_right->prev = node1;
            node1->next = node2_right;
       } else {
            tail = node2;
           node1->next = nullptr;
       }
        return;
    //validate();
    }
    */
   //split
    /*

    void DoubleLinkedList::swap(Node* node1, Node* node2) {
    if (node1 == node2)
        return;

    if (node1->next == node2) { // right next to each other
        node1->next = node2->next;
        node2->prev = node1->prev;

        if (node1->next != nullptr)
            node1->next->prev = node1;

        if (node2->prev != nullptr)
            node2->prev->next = node2;


        node2->next = node1;
        node1->prev = node2;
    } else {
        Node* p = node2->prev;
        Node* n = node2->next;

        node2->prev = node1->prev;
        node2->next = node1->next;

        node1->prev = p;
        node1->next = n;

        if (node2->next != nullptr)
            node2->next->prev = node2;
        if (node2->prev != nullptr)
            node2->prev->next = node2;

        if (node1->next != nullptr)
            node1->next->prev = node1;
        if (node1->prev != nullptr)
            node1->prev->next = node1;

    }
}*//*
    void DoubleLinkedList::swap(Node* node1, Node* node2){
        Node* temp;
        temp = node2->next;
        node2->next = node1->next;
        node1->next = temp;

        if (node1->prev == nullptr){
            node2 = head;
            node2->prev->next = node1;
        }
        if (node2->prev == nullptr){
            node1 = head;
            node1->prev->next = node2->prev;
        }
        if (node1->prev != nullptr && node2->prev != nullptr){
            node1->prev->next = node2;
            node2->prev->next = node1;
        } 
        if (node1->next == nullptr){
            node2 = tail;
            node2->next->prev = node1;
        }
        if (node2->next == nullptr){
            node1 = tail;
            node1->next->prev = node2;
        }
        if(node1 == head && node2->next == tail){
            node1->prev->next = tail;
            node2->next->prev = head;
        }
        if(node2 == head && node1 == tail){
            node2->prev->next = tail;
            node1->next->prev = head;
        }
            else {
            return;
        }
    }


void DoubleLinkedList::swap(Node* node1, Node* node2){
    if (node1->prev){
        node1->prev->next = node2;
    } else {
        head = node2;
    }
    if (node2->next){
        node2->next->prev = node1;
    }
    node1->next = node2->next;
    node2->prev = node1->prev;
    node2->next = node1;
    node1->prev = node2;
}

void DoubleLinkedList::swap ( Node *a1, Node *a2 ) {

Node* a1p = a1->prev;
Node* a2p = a2->prev;

Node* a2n_o = a2->next;
Node* a1n_o = a1->next;

Node* a1n = a1->next;
Node* a2n = a2->next;

Node* a2p_o = a2->prev;
Node* a1p_o = a1->prev;

if ( a1->next == a2 ) {
// ...->[ a1 ]->[ a2 ]->...
if ( a1p != NULL ) {
a1p->next = NULL;
}

if ( a2n != NULL ) {
a2n->prev = NULL;
}

a2->next = a1;
a1->next = a2n_o;

a1->prev = a2;
a2->prev = a1p_o;

if ( a1p != NULL ) {
a1p->next = a2;
}

if ( a2n != NULL ) {
a2n->prev = a1;
}

} else if ( a2->next == a1 ) {
// ...->[ a2 ]->[ a1 ]->...
if ( a2p != NULL ) {
a2p->next = NULL;
}

if ( a1n != NULL ) {
a1n->prev = NULL;
}

a1->next = a2;
a2->next = a1n_o;

a2->prev = a1;
a1->prev = a2p_o;

if ( a2p != NULL ) {
a2p->next = a1;
}

if ( a1n != NULL ) {
a1n->prev = a2;
}
} else {
// ...->[ a1 ]->...->[ a2 ]->...
if ( a2p != NULL ) {
a2p->next = NULL;
}

if ( a1n != NULL ) {
a1n->prev = NULL;
}

if ( a1p != NULL ) {
a1p->next = a2;
}

if ( a2n != NULL ) {
a2n->prev = a1;
}

if ( a2p != NULL ) {
a2p->next = a1;
}

if ( a1n != NULL ) {
a1n->prev = a2;
}

a1->next = NULL;
a2->next = a1n_o;
a1->next = a2n_o;

a2->prev = NULL;
a1->prev = a2p_o;
a2->prev = a1p_o;
}

}


    void DoubleLinkedList::swap(Node* node1, Node* node2){
    Node* temp1 = tail;
    Node* temp2 = head;
    if(node1->prev == nullptr) node2->prev->next = temp1;
    if(node2->next == nullptr) node1->next->prev = temp2;
    if(node1->prev)  node1->prev->next = node2;
    if(node2->prev)  node2->prev->next = node1;
    if(node1->next) node1->next->prev= node2;
    if(node2->next) node2->next->prev= node1;
    Node* temp;
    temp = node1->prev;
    node1->prev = node2->prev;    
    node2->prev = temp;
    temp = node1->next;
    node1->next= node2->next;    
    node2->next= temp;
    }*/

        const bool DoubleLinkedList::isSorted() const{
        if(nodeCount <= 1)
            return true;

        for(Node* i = head; i->next != nullptr; i = i->next){
            if(*i > *i->next)
            return false;
        }
        return true;
    }

     void DoubleLinkedList::insertionSort(){
          Node *current = NULL, *index = NULL; 
    Node* temp;  
    if(head == NULL) { 
        return; 
    } 
    else { 
        for(current = head; current->next != NULL; current = current->next) { 
 
            for(index = current->next; index != NULL; index = index->next) { 
   
                if(current > index) { 
                    temp = current; 
                    current = index; 
                    index = temp; 
                   
                     } 
            } 
        } 
    } 
     }

  /*  void DoubleLinkedList::insertionSort(){    
        Node *marker, *current;

    for (marker = head->next; marker != nullptr; marker = marker->next)
    {
        Node* temp = marker;                                 
        current = marker;   

        // this line throws the exception: read access violation.
        // current->prev was nullptr.                                       
        while (current != nullptr && current->prev >= temp) 
        {
            current = current->prev;            
            current = current->prev;                       
        }
        current = temp;                  
      }  
   }
*/
   /* void sortedInsert(Node* currentNode, Node* newNode)
{
    Node* current;
  
    // if list is empty
    if (currentNode == NULL)
        currentNode = newNode;
  
    // if the node is to be inserted at the beginning
    // of the doubly linked list
    else if ((currentNode) >= newNode) {
        newNode->next = currentNode;
        newNode->next->prev = newNode;
        currentNode = newNode;
    }
  
    else {
        current = currentNode;
  
        // locate the node after which the new node
        // is to be inserted
        while (current->next != NULL && 
               current->next < newNode)
            current = current->next;
  
        newNode->next = current->next;
  
        // if the new node is not inserted
        // at the end of the list
        if (current->next != NULL)
            newNode->next->prev = newNode;
  
        current->next = newNode;
        newNode->prev = current;
    }
}



    void DoubleLinkedList::insertionSort(){
        Node* sorted = nullptr;

        Node* current = head;
        while(current != nullptr){
            Node* next = current->next;
            current->prev = current->next = nullptr;

            sortedInsert(&sorted, current);

            current = next;
        }

        head = sorted;
    }*/


    bool DoubleLinkedList::validate() const {
        if( head == nullptr ) {
            assert(tail == nullptr);
            assert(nodeCount == 0);
        }else {
            assert(tail != nullptr);
            assert(nodeCount != 0);
        }

        if( tail == nullptr ) {
            assert(head == nullptr);
            assert(nodeCount == 0);
        } else {
            assert(head != nullptr);
            assert(nodeCount != 0);
        }
        if (head != nullptr && tail == head ){
            assert(nodeCount == 1);
        }

        unsigned int forwardCount = 0;
        Node* currentNode = head;

        while(currentNode != nullptr){
            forwardCount++;
            if(currentNode->next != nullptr){
                assert(currentNode->next->prev == currentNode);
            }
            currentNode = currentNode->next;
            }
            assert(nodeCount == forwardCount);

        unsigned int backwardCount = 0;
        currentNode = tail;
        while(currentNode != nullptr){
            backwardCount++;
            if(currentNode->prev != nullptr){
                assert(currentNode->prev->next == currentNode);
            }
            currentNode = currentNode->prev;
        }
        assert(nodeCount == backwardCount);
        // cout << "List is valid" << endl;
        return true;
        }

        void DoubleLinkedList::dump() const {
            cout << "DoubleLinkedList: head=[" << head << "]    tail=[" << "]" <<endl;
            for(Node* currentNode = head; currentNode !=nullptr; currentNode = currentNode->next){
                cout << "  ";
                currentNode->dump();
            }
        }


///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 2.0
///
/// Unit test for this lab
///
/// @author @todo Jayson Iwanaka <@todo jiwanaka@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   @todo 20_04_2021
///////////////////////////////////////////////////////////////////////////////


#pragma once

#include "node.hpp"

    class DoubleLinkedList : public Node{
        protected:
        Node* head = nullptr;
        Node* tail = nullptr;

        public:
        const bool empty() const ;
        const bool isIn(Node* aNode) const;
        void push_front( Node* newNode );
        void push_back( Node* newNode);
        Node* pop_front() ;
        Node* pop_back();
        Node* get_first() const;
        Node* get_last() const;
        Node* get_next( const Node* currentNode ) const ;
        Node* get_prev( const Node* currentNode ) const;
        inline unsigned int size() const{return nodeCount;};
        void insert_after(Node* currentNode, Node* newNode);
        void insert_before(Node* currentNode, Node* newNode);
        void sortedInsert(Node* currentNode, Node* newNode);
        void swap(Node* node1, Node* node2);
        const bool isSorted() const;
        void insertionSort();
        bool validate() const;
        void dump() const;


        



    };
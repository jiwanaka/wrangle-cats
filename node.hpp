///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file node.hpp
/// @version 2.0
///
/// Unit test for this lab
///
/// @author @todo Jayson Iwanaka <@todo jiwanaka@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   @todo 20_04_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

class Node {
friend class DoubleLinkedList;

protected:
	Node* next = nullptr;
	Node* prev = nullptr;
	unsigned int nodeCount = 0;

public:
	virtual bool operator>(const Node& r);
	void dump() const;

}; // class Node

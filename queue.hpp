///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file queue.hpp
/// @version 2.0
///
/// Unit test for this lab
///
/// @author @todo Jayson Iwanaka <@todo jiwanaka@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   @todo 20_04_2021
///////////////////////////////////////////////////////////////////////////////

#include "node.hpp"
#include "list.hpp"

#pragma once

class Queue {
protected:
	DoubleLinkedList list = DoubleLinkedList();
	//Node* ptr = nullptr; 
	//const Node* ptr2 = nullptr;
public:
	inline const bool empty() const { return list.empty(); };
	inline unsigned int size() const { return list.size(); };
	inline void push_front(Node* newNode) {return list.push_front(newNode);};
	inline Node* pop_back() { return list.pop_back(); };
	inline Node* get_first() const { return list.get_first(); };
	inline Node* get_next(const Node* currentNode) const { return list.get_next(currentNode); };

	// Using the above as an example, bring over the following methods from list:
	// Add size()
	// Add push_front()
	// Add pop_back()
	// Add get_first()
	// Add get_next()

}; // class Queue
